import 'dart:async';

import 'package:flutter/services.dart';

class PlayCoreReviews {
  static const MethodChannel _channel = const MethodChannel('play_core_reviews');

  static Future<bool> prepareRequestReview() async {
    try {
      bool response = await _channel.invokeMethod("prepareRequestReview");
      return response;
    } catch (exception) {
      if (exception is PlatformException) {
        print(exception.message);
      }
      return false;
    }
  }

  static Future<bool> requestReview() async {
    try {
      bool response = await _channel.invokeMethod("requestReview");
      return response;
    } catch (exception) {
      if (exception is PlatformException) {
        print(exception.message);
      }
      return false;
    }
  }
}
