package com.jakubmateusiak.play_core_reviews

import android.app.Activity
import android.content.Context
import android.util.Log
import androidx.annotation.NonNull
import com.google.android.play.core.review.ReviewInfo
import com.google.android.play.core.review.ReviewManager
import com.google.android.play.core.review.ReviewManagerFactory
import com.google.android.play.core.review.testing.FakeReviewManager

import io.flutter.embedding.engine.plugins.FlutterPlugin
import io.flutter.embedding.engine.plugins.activity.ActivityAware
import io.flutter.embedding.engine.plugins.activity.ActivityPluginBinding
import io.flutter.plugin.common.MethodCall
import io.flutter.plugin.common.MethodChannel
import io.flutter.plugin.common.MethodChannel.MethodCallHandler
import io.flutter.plugin.common.MethodChannel.Result
import io.flutter.plugin.common.PluginRegistry.Registrar

/** PlayCorePlugin */
class PlayCoreReviewsPlugin : FlutterPlugin, MethodCallHandler, ActivityAware {
    /// The MethodChannel that will the communication between Flutter and native Android
    ///
    /// This local reference serves to register the plugin with the Flutter Engine and unregister it
    /// when the Flutter Engine is detached from the Activity
    private final val TAG = "PlayCorePlugin"
    private lateinit var channel: MethodChannel
    private lateinit var context: Context
    private var activity: Activity? = null
    private var reviewInfo: ReviewInfo? = null

    override fun onAttachedToEngine(@NonNull flutterPluginBinding: FlutterPlugin.FlutterPluginBinding) {
        channel = MethodChannel(flutterPluginBinding.binaryMessenger, "play_core_reviews")
        channel.setMethodCallHandler(this)
        context = flutterPluginBinding.applicationContext
    }

    override fun onMethodCall(@NonNull call: MethodCall, @NonNull result: Result) {
        when (call.method) {
            "getPlatformVersion" -> {
                result.success("Android ${android.os.Build.VERSION.RELEASE}")
            }
            "requestReview" -> {
                requestReview(context, result)
            }
            "prepareRequestReview" -> {
                prepareRequestReview(context, result)
            }
            else -> {
                result.notImplemented()
            }
        }
    }

    override fun onDetachedFromEngine(@NonNull binding: FlutterPlugin.FlutterPluginBinding) {
        channel.setMethodCallHandler(null)
    }


    private fun prepareRequestReview(context: Context, @NonNull result: Result) {
        val manager = ReviewManagerFactory.create(context)
        val request = manager.requestReviewFlow()
        request.addOnCompleteListener {
            if (it.isSuccessful) {
                reviewInfo = it.result
                result.success(true)
            } else {
                result.error("500", it.exception?.message, null)
            }
        }
    }

    private fun requestReview(context: Context, @NonNull result: Result) {
        val manager = ReviewManagerFactory.create(context)
        if (reviewInfo != null && activity != null) {
            val flow = manager.launchReviewFlow(activity!!, reviewInfo!!)
            flow.addOnCompleteListener { status ->
                result.success(true)
            }
        } else {
            result.error("500", "No review info", null)
        }
    }

    override fun onDetachedFromActivity() {
        Log.d("TAG", "onDetachedFromActivity")
        activity = null
    }

    override fun onReattachedToActivityForConfigChanges(binding: ActivityPluginBinding) {
        Log.d("TAG", "onReattachedToActivityForConfigChanges")
    }

    override fun onAttachedToActivity(binding: ActivityPluginBinding) {
        activity = binding.activity
        Log.d("TAG", "onAttachedToActivity")
    }

    override fun onDetachedFromActivityForConfigChanges() {
        Log.d("TAG", "onDetachedFromActivityForConfigChanges")
    }


}
