import 'package:flutter/material.dart';
import 'dart:async';

import 'package:flutter/services.dart';
import 'package:play_core/play_core.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  String _platformVersion = 'Unknown';

  @override
  void initState() {
    super.initState();
    PlayCoreReviews.prepareRequestReview();
  }


  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        appBar: AppBar(
          title: const Text('Plugin example app'),
        ),
        body: Center(
          child: Column(
            children: [
              Text('Play core in-app review test'),
              MaterialButton(
                child: Text("Click me"),
                onPressed: () async {
                  print("request review: " + (await PlayCoreReviews.requestReview()).toString());
                },
              )
            ],
          ),
        ),
      ),
    );
  }
}
